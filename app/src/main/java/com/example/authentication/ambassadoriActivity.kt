package com.example.authentication

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.icu.util.Calendar
import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.ambassadori.*
import java.util.*
import kotlinx.android.synthetic.main.ambassadori.textView15 as textView151

class ambassadoriActivity:AppCompatActivity(), DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    var day = 0
    var month = 0
    var year = 0
    var hour = 0
    var minute = 0

    var savedday = 0
    var savedmonth = 0
    var savedyear = 0
    var savedhour = 0
    var savedminute = 0
    override fun dispatchKeyEvent(event: KeyEvent?): Boolean {
        TODO("Not yet implemented")
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.ambassadori)
        pickDate()
    }
    @RequiresApi(Build.VERSION_CODES.N)
    private fun getDateTimeCaldendar(){
        val cal :Calendar = Calendar.getInstance()
        day = cal.get(Calendar.DAY_OF_MONTH)
        month = cal.get(Calendar.MONTH)
        year = cal.get(Calendar.YEAR)
        hour = cal.get(Calendar.HOUR)
        minute = cal.get(Calendar.MINUTE)
    }
    private fun pickDate(){
        pickDate1.setOnClickListener {
            getDateTimeCaldendar()

            DatePickerDialog(this,this,year,month,day).show()
        }
    }
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth:Int){
        savedday = dayOfMonth
        savedmonth = month
        savedyear = year

        getDateTimeCaldendar()
        TimePickerDialog(this, this,hour,minute, true).show()

    }
    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int){
        savedhour = hourOfDay
        savedminute = minute


        textView151.text = "$savedday-$savedmonth-$savedyear-$savedhour-$savedminute"
    }

}